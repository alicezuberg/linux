# NOTE: Linux Singleplayer is Maintained by Volunteers.
This subproject is maintained exclusively by volunteers, and will not receive official support from the core 2009scape team. If you do have issues, however, feel free to make an issue in this repository and hopefully a volunteer will be able to help you out :). 


# Getting Started: Windows
See  https://github.com/2009scape/Singleplayer-Edition-Windows

# Getting Started: Linux
* 1: Download or clone this repository.
* 2: After you unpack the .zip (if you just downloaded it) or it finishes cloning (if you cloned it), open up the folder that it created.
* 3: In this folder there will be a `launch-server.sh`, execute this to start the server.
* 4: Once you see the message "[INFO] 2009scape started in xxx milliseconds.", you can safely start the client with `launch-client.sh`.
